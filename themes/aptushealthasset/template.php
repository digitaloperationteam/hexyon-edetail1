<?php

function aptushealthasset_preprocess_page(&$variables) {
	global $user;
	
	$theme_path = path_to_theme();
	$site_name = variable_get('site_name');
	$site_name = strtolower($site_name);
	$server_url = 'http://' . $_SERVER['HTTP_HOST'];
	if ($user->uid == 0 && arg(0) == 'user') {
		drupal_add_css($theme_path . '/css/login.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
	} else {
		drupal_add_css('http://cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
		drupal_add_css($theme_path . '/player/css/styles.min.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
		drupal_add_css($theme_path . '/app/css/main.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
	
		$html5shiv = array(
			'#tag' => 'script',
			'#attributes' => array( // Set up an array of attributes inside the tag
				'src' => $server_url . '/' . $site_name . '/' . $theme_path . '/player/js/html5shiv.js',
			),
			'#prefix' => '<!--[if lte IE 9]>',
			'#suffix' => '</script><![endif]-->',
		);
		drupal_add_html_head($html5shiv, 'html5shiv');
		
		drupal_add_js('http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
		drupal_add_js('http://cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
		//drupal_add_js($theme_path . '/player/js/main.min.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/player/js/modernizr.min.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/player/js/head.min.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/player/js/customEvent.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/player/js/classList.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/player/js/reveal.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/app/js/config_player.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/app/js/main.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/app/js/brightcove_popin.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
		drupal_add_js($theme_path . '/app/js/gsapAnimation.js', array('group' => JS_THEME, 'preprocess' => TRUE, 'scope' => 'footer', 'weight' => '999'));
	}
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function aptushealthasset_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  $theme_path = path_to_theme();
  drupal_add_css($theme_path . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function aptushealthasset_process_maintenance_page(&$variables) {
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}


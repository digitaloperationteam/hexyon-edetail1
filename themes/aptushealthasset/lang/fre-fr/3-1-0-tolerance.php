﻿<section id="tolerance-3-1-0" class="tolerance" data-background="images/bg-presentation.jpg" data-slide-name="Tol&eacute;rance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s005" data-screen-label="Profil de tol&eacute;rance - Analyse" data-screen-name="Tol&eacute;rance" data-screen-section="Analyse" data-menu-title="Tol&eacute;rance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>un bon profil de tol&eacute;rance</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2>Analyse int&eacute;gr&eacute;e de la tol&eacute;rance des essais de primovaccination comparatifs &agrave; Infanrix<sup>&reg;</sup> Hexa</h2>
            		<p class="text-center">R&eacute;actions &ldquo;sollicit&eacute;es&rdquo; apr&egrave;s primovaccination - Population de Tol&eacute;rance</p>
                    <img src="images/tolerance_tab1.png" alt="Analyse de tol&eacute;rance" width="812" height="311">
                    <p class="legend">Analyse int&eacute;gr&eacute;e des essais comparatifs &agrave; Infanrix Hexa (A3L11, A3L12, A3L17, A3L24, A3L38)<br>Population de Tol&eacute;rance<br>R&eacute;actions pouvant &ecirc;tre rapport&eacute;es pour chacun des vaccins (Hexavalent ou Prevenar/Prevenar 13 en cas de co-administration)</p>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn popin">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Risk Management Plan V 10.0 - Rapport d&rsquo;analyse int&eacute;gr&eacute;e de la tol&eacute;rance.</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn pdfDownload">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn pdfDownload">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="225" height="65">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
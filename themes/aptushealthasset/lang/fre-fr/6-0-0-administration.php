﻿<section id="administration-6-0-0" class="administration" data-background="images/bg-presentation.jpg" data-slide-name="Sch&eacute;ma d'administration" data-mainsection-title="Sch&eacute;ma d'administration" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s011" data-screen-label="Sch&eacute;ma d'administration" data-screen-name="Sch&eacute;ma d'administration" data-screen-section="" data-menu-title="Sch&eacute;ma d'administration">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Hexyon<sup>&reg;</sup> est recommand&eacute; <sup>1</sup><br>et rembours&eacute; &agrave; 65%</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
                    <h3 class="text-center">Utilisation selon le calendrier vaccinal fran&ccedil;ais <sup>1</sup> &agrave; 2, 4 et 11 mois</h3>
                    <img src="images/administration-hexyon.png" class="responsive" alt="Sch&eacute;ma d'administration" width="924" height="198">
                    
                    <div class="column">
                        <div class="col-left">
                            <div class="whiteRadiusBox">
                                <h3 class="text-center">Co-administration possible avec vaccin pneumococcique conjugu&eacute; <sup>2</sup></h3>
                                <p class="text-center">En 2 sites d&rsquo;injection distincts</p>
                            </div>
                        </div>
                        <div class="col-right">
                            <div class="whiteRadiusBox">
                                <h3 class="text-center">Utilisation possible en dose de rappel <sup>2</sup></h3>
                                <p class="text-center">Quel que soit le vaccin re&ccedil;u en primovaccination</p>
                                <a href="#" class="blueBtn popin" data-featherlight="#utilisation">En savoir plus</a>
                            </div>
                        </div>
                        
                    </div>

                    <div id="utilisation" class="feather">
                        <h2 class="uppercase">Utilisation possible en dose de rappel<sup>1</sup></h2>
                        <p class="big">Quelque soit le vaccin re&ccedil;u en primovaccination :</p>
                        <div class="column">
                            <div class="col-left">
                                <h3 class="uppercase normal">PRIMOVACCINATION (2,4 mois)</h3>
                            </div>
                            <div class="col-right">
                                <h3 class="uppercase normal">Rappel (11 mois)</h3>
                            </div>
                        </div>
                        <img src="images/hexyon-utilisation.jpg" class="responsive" alt="Utilisation d'Hexyon" width="892" height="139">
                        <p class="legend">1. Calendrier vaccinal 2016 :<br><a target="_blank" href="http://social-sante.gouv.fr/IMG/pdf/calendrier_vaccinal_2016.pdf">http://social-sante.gouv.fr/IMG/pdf/calendrier_vaccinal_2016.pdf (acc&eacute;d&eacute; le 21/07/2016)</a></p>  
                    </div>

                    <div id="methodo" class="feather">
                        <h2 class="uppercase">CONTRE-INDICATIONS<sup>1</sup></h2>
                        <ul>
                           <li>Ant&eacute;c&eacute;dent de r&eacute;action anaphylactique apr&egrave;s une pr&eacute;c&eacute;dente administration d&rsquo;Hexyon.</li>
                           <li>Hypersensibilit&eacute; aux substances actives, &agrave; l&rsquo;un des excipients, &agrave; des r&eacute;sidus &agrave; l'&eacute;tat de traces (glutarald&eacute;hyde, formald&eacute;hyde, n&eacute;omycine, streptomycine et polymyxine B), &agrave; un vaccin coquelucheux, ou hypersensibilit&eacute; suite &agrave; une pr&eacute;c&eacute;dente administration d&rsquo;Hexyon ou d'un vaccin contenant les m&ecirc;mes composants. </li>
                           <li>Personnes ayant pr&eacute;sent&eacute; une enc&eacute;phalopathie d&rsquo;&eacute;tiologie inconnue, survenue dans les 7 jours suivant l&rsquo;administration d&rsquo;un vaccin coquelucheux (vaccins coquelucheux &agrave; germes entiers ou acellulaires). Dans ce cas, la vaccination contre la coqueluche doit &ecirc;tre interrompue et le sch&eacute;ma de vaccination doit &ecirc;tre poursuivi avec des vaccins dipht&eacute;rie, t&eacute;tanos, h&eacute;patite B, poliomy&eacute;lite et Hib.</li>
                           <li>Sujets pr&eacute;sentant des troubles neurologiques non contr&ocirc;l&eacute;s ou une &eacute;pilepsie non contr&ocirc;l&eacute;e avant qu&rsquo;un traitement n&rsquo;ait &eacute;t&eacute; mis en place, que l'&eacute;tat du patient n&rsquo;ait &eacute;t&eacute; stabilis&eacute; et que le b&eacute;n&eacute;fice ne soit clairement sup&eacute;rieur au risque.</li>
                        </ul>
                        <p class="legend">1. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</p> 
                    </div>
                    
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn popin">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Avis du Haut Conseil de la Sant&eacute; Publique du 20 f&eacute;vrier 2015 relatif &agrave; l'utilisation du vaccin hexavalent HEXYON&reg; destin&eacute; &agrave; la primo-vaccination et &agrave; la vaccination du rappel des nourrissons contre la dipht&eacute;rie, le t&eacute;tanos, la coqueluche, l'h&eacute;patite B, la poliomy&eacute;lite et les infections invasives &agrave; <i>Haemophilus influenzae</i> type b.</li>
                        <li class="no-bullet">2. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</li>
					</ul>
				</div>
                <button id="contreIndications" class="footerBtn popin" data-featherlight="#methodo">Contre-indications</button>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn pdfDownload">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn pdfDownload">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="225" height="65">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
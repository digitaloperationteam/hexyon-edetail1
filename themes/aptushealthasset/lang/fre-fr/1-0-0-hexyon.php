﻿<section id="hexyon-1-0-0" class="hexyon" data-background="images/bg-hexyon3mots.jpg" data-mainsection-title="Hexyon<sup>&reg;</sup> en 3 mots" data-slide-name="Hexyon en 3 mots" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s002" data-screen-label="Hexyon en 3 mots" data-screen-name="Hexyon en 3 mots" data-screen-section="" data-menu-title="Hexyon en 3 mots">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Hexyon<sup>&reg;</sup>, le vaccin hexavalent<br>sans reconstitution<sup>2</sup></h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	
            	<div class="content">
            		<div class="column">
            			<div class="col-left">
            				<img src="images/hexyon-3mots.png" alt="Hexyon en 3 mots" width="343" height="113">
            			</div>
            			<div class="col-right">
            				<a href="#/immunogenicite-2-0-0" id="h3m-b1" class="bubble">
            					<p><strong>Immunog&eacute;nicit&eacute;</strong><br>
            					R&eacute;ponse immunitaire &eacute;lev&eacute;e<sup>1</sup><br>
            					<i class="fa fa-angle-right" aria-hidden="true"></i>
            					<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            				</a>
            				<a href="#/tolerance-3-0-0" id="h3m-b2" class="bubble">
            					<p><strong>Tol&eacute;rance<sup>2</sup><br>& Exp&eacute;rience</strong><br>
            					<i class="fa fa-angle-right" aria-hidden="true"></i>
            					<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            				</a>
            				<a href="#/serenite-4-0-0" id="h3m-b3" class="bubble">
            					<p><strong>S&eacute;r&eacute;nit&eacute;</strong><br>
            					Simplicit&eacute; & S&eacute;curit&eacute; d&rsquo;emploi<sup>2,3,4</sup><br>
            					<i class="fa fa-angle-right" aria-hidden="true"></i>
            					<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            				</a>
            				<img src="images/hexyon-vaccin_01.png" alt="Hexyon vaccin" width="382" height="217">
            			</div>
            		</div>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn popin">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Rapport d'&eacute;tude A3L38. EudraCT #2012-001054-26.</li>
						<li class="no-bullet">2. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</li>
						<li class="no-bullet">3. Schmid DA, et al. Development and introduction of a ready-to-use pediatric pentavalent vaccine to meet and sustain the needs of developing countries &ndash; Quinvaxem&reg;: The first 5 years. Vaccine&nbsp;2012;30(44):6241-8.</li>
						<li class="no-bullet">4. De Coster I, et al. Assessment of preparation time with fully-liquid versus non-fully liquid paediatric hexavalent vaccines. A time and motion study. Vaccine 2015;33(32):3976-82.</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn pdfDownload">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn pdfDownload">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="225" height="65">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
﻿<section id="tolerance-3-2-0" class="tolerance" data-background="images/bg-presentation.jpg" data-slide-name="Tol&eacute;rance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s006" data-screen-label="Profil de tol&eacute;rance - Effets ind&eacute;sirables - 1" data-screen-name="Tol&eacute;rance" data-screen-section="Effets ind&eacute;sirables - 1" data-menu-title="Tol&eacute;rance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>un bon profil de tol&eacute;rance</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2>Effets ind&eacute;sirables les plus fr&eacute;quemment rapport&eacute;s<br> dans les essais cliniques et apr&egrave;s la mise sur le march&eacute;<sup>1</sup> :</h2>
            		<div class="column">
                        <div class="col-left">
                            <div class="whiteRadiusBox">
                                <h3>Syst&eacute;miques</h3>
                                <ul>
                                    <li>Anorexie (diminution de l'app&eacute;tit), pleurs, somnolence, vomissements, irritabilit&eacute;, fi&egrave;vre &ge; 38,0&deg;C</li>
                                    <li>Pleurs anormaux (prolong&eacute;s) et Diarrh&eacute;e</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-right">
                            <div class="whiteRadiusBox">
                                <h3>Au site d&rsquo;injection</h3>
                                <ul>
                                    <li>Douleur, &eacute;ryth&egrave;me et &oelig;d&egrave;me</li>
                                    <li>Induration</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <p>Une r&eacute;actog&eacute;nicit&eacute; sollicit&eacute;e l&eacute;g&egrave;rement sup&eacute;rieure a &eacute;t&eacute; observ&eacute;e apr&egrave;s la premi&egrave;re dose, par rapport aux doses suivantes.<br><span class="medium">Pour plus d'information sur les effets ind&eacute;sirables peu fr&eacute;quents ou rares, consultez le R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</span></p>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn popin">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn pdfDownload">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn pdfDownload">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="225" height="65">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
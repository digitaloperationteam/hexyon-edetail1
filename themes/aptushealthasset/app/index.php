<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hexyon</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!--build:remove -->
        <link rel="stylesheet" href="../../../_player/css/normalize.min.css">
        <link rel="stylesheet" href="../../../_player/css/reveal.css">
        <link rel="stylesheet" href="../../../_player/css/animate.css">
        <link rel="stylesheet" href="../../../_player/css/main.css">
        <!-- endbuild -->

        <!--build:css css/styles.min.css-->
        <link rel="stylesheet" href="css/main.css">
        <!--endbuild-->

        <!--[if lte IE 9]>
            <script src="../../../_player/js/vendor/html5shiv.js"></script>
        <![endif]-->
    </head> 
    <body>

        <div class="reveal">
            
            <div class="slides">
               
                <?php

                    function recursiveDirectoryInclude($dir) {
                        $cdir = scandir($dir);
                        foreach ($cdir as $key => $value) {
                         if (!in_array($value, array('.', '..', '.DS_Store', '._.DS_Store'))) {
                          if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                            recursiveDirectoryInclude($dir . DIRECTORY_SEPARATOR . $value);
                          }
                          else {
                              include $dir . DIRECTORY_SEPARATOR . $value;
                          }
                         }
                        }
                    }

                    recursiveDirectoryInclude('inc');
                ?>
            </div>

        </div>

        
        <!--build:remove -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../../_player/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
        <script src="../../../_player/js/vendor/modernizr.min.js"></script>
        <script src="../../../_player/js/vendor/head.min.js"></script>
        <script src="../../../_player/js/vendor/classList.js"></script>
        <script src="../../../_player/js/vendor/customEvent.js"></script>
        <script src="../../../_player/js/reveal.js"></script>
        <!-- endbuild -->

        <!--build:js js/main.min.js -->
        <script src="js/config_player.js"></script>
        <script src="js/main.js"></script> 
        <script src="js/gsapAnimation.js"></script> 
        <!-- endbuild -->
        
        <script>

        </script>

    </body>
</html>

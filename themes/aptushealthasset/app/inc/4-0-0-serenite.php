<section id="serenite-4-0-0" class="serenite" data-background="images/bg-presentation.jpg" data-slide-name="Sérénité" data-mainsection-title="3. Sérénité" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s008" data-screen-label="Une simplicité d'utilisation" data-screen-name="Sérénité" data-screen-section="Utilisation" data-menu-title="Sérénité">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Une simplicité d’utilisation</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
                    <img src="images/hexyon-vaccin_01.png" alt="Hexyon vaccin" width="382" height="217">
            		<div class="whiteBubble">
                        <p>Un vaccin<br>prêt à l’emploi<br><span class="bold">=<br>Gain de temps<sup>2</sup></span></p>      
                    </div>
                    <ul class="hexyon">
                        <li>Une formulation unique : 100 % liquide<sup>1</sup></li>
                        <li>En seringue pré-remplie<sup>1</sup></li>
                        <li>Absence de reconstitution<sup>1</sup></li>
                        <li>2 aiguilles au choix pour l’injection<sup>1</sup></li>
                    </ul>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn popin">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Résumé des Caractéristiques du Produit</li>
                        <li class="no-bullet">2. De Coster I, et al. Assessment of preparation time with fully-liquid versus non-fully liquid paediatric hexavalent vaccines. A time and motion study. Vaccine 2015;33(32):3976-82.</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn pdfDownload">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn pdfDownload">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="225" height="65">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
(function(jQuery){	
	
    var popin = jQuery('.BC-playlistOverlay'),
        ctaOpen = jQuery('.BC-openPopin'),
        ctaClose = jQuery('.BC-closePopin'),
        videoBrightcove = jQuery('.BC-playlistOverlay video');

    //Add eventlistener
    ctaOpen.click(function(e){
        
        showVideoOverlay();
        return false;
    });

    popin.click(function(){
        event.stopPropagation();
    });

    jQuery('body').click(function(){
        hideVideoOverlay();
    });

    jQuery('span.BC-close-popin').click(function(){
        hideVideoOverlay();
    });

    //Function to show the overlay
    function showVideoOverlay(){
        /*popin.fadeTo("slow", "1", function(){
            jQuery(this).find('video')[0].play();
        });*/
        jQuery('.BC-playlistOverlay').css("display", "block");
        videojs("BC-playerHTML5").play();
        //popin.find('video')[0].play();
    }

    //Function to hide the overlay
    function hideVideoOverlay(){
        /*popin.fadeTo('slow', '0', function(){
            jQuery(this).find('video')[0].pause();
        });*/
        //videoBrightcove.pause();
        
        jQuery('.BC-playlistOverlay').css("display", "none");
        videojs("BC-playerHTML5").pause();
        
    }

})(jQuery);
(function($){	
	//To catch the event of slide change.
	Reveal.addEventListener( 'slidechanged', function( event ) {
	    // event.previousSlide, event.currentSlide, event.indexh, event.indexv
	    var currentSlide = event.currentSlide;
	    var previousSlide = event.currentSlide;
	    var currentSlideIndex = event.indexh;
        
        //CloseSafety
        closeOverlay();
	    
	});
    
    /********************
    *****SafetyPopin****
    ********************/
    
    $('.efficacy .safetyCircle').on('click', function(e){
        openOverlay(this, '#efficacy-4-5-1');
        e.stopPropagation();
    });

    $('#side-effects-5-2-1 .safetyCircle, #side-effects-5-2-2 .safetyCircle, #side-effects-5-3-1 .safetyCircle, #side-effects-5-3-2 .safetyCircle, #side-effects-5-4-1 .safetyCircle, #side-effects-5-4-2 .safetyCircle')
    .on('click', function(e){
        openOverlay(this, '#side-effects-5-6-1');
        e.stopPropagation();
    }); 
    $('#side-effects-5-5-1 .safetyCircle, #side-effects-5-5-2 .safetyCircle')
    .on('click', function(e){
        openOverlay(this, '#side-effects-5-6-2');
        e.stopPropagation();
    }); 
    
    //GENERAL
    $('.contentSection').on('click', function(){
        closeOverlay(1500);
    });
    
    $('.closePopinBtn').on('click', function(){
        closeOverlay(1500);
    })

    //SPAIN
    $('#side-effects-5-3-1 .graph').wrap("<div class='scoll'></div>");
    $('#side-effects-5-4-1 .graph').wrap("<div class='scoll'></div>");
    
    function openOverlay(el, target){
        if(!$(el).hasClass('active')){
            $(target + ' .safetyInformation').clone().appendTo('.safetyInfoButton .safetyCircle');
            $('.safetyInfoButton').addClass('active');
            $('.safetyInfoButton .safetyCircle').addClass('active');
            $('.safetyInfoButton .label').fadeOut("1000", function(){});
            $('.safetyInfoButton').animate({
                'height': '960px',
                'bottom': '-102px'
                }, 1000, function(){

                }
            );        
            //ADD CLOSE BTN
            $('<div class="closePopinBtn"><span><i class="fa fa-times" aria-hidden="true"></i></span></div>').prependTo('.safetyInfoButton');
        }
    }
    
    function closeOverlay(delay){
        if(!delay){
            var delay = 0;
        }
        $(".safetyInfoButton").css("top","");
        $('.safetyInfoButton').animate({
            'height': '50px',
            'bottom': '0'
            }, delay, function(){
                //ANIMATION COMPLETED
                $('.safetyInfoButton').removeClass('active');
                $('.safetyInfoButton .safetyCircle').removeClass('active');
                $('.safetyInfoButton .safetyInformation').remove();
                $('.safetyInfoButton .label').fadeIn("1500");
            }
        );     
    }
    

    
    

})(jQuery);
jQuery(function(){


	Reveal.initialize({
	    autoplay: false,
		dependencies: [
			{ src: '../sites/hexion-edetail1/themes/hexion/player/js/menu/menu.js' }
	    ],
	    width: '100%',
    	height: '100%',
	    menu: {
	    	custom: false,
	    	themes: false,
	    	transitions: false
	    },
	    backgroundTransition: 'slide'
	});

	function slideProgressChapters(){

		var progressBarSize = '';
		var nbOfSlides = jQuery(".slides > section").length;
		var btnPos = '';
		var currentPos = 1;
		var leftBtnPos = 0;
		var counter = 0;
		    
		    progressBarSize = jQuery(".reveal .progress").width();

		    jQuery('.progress>span .slideButton').remove();
			
			jQuery('.slides>section').each(function(){
				counter = jQuery(this).prevAll().length;
				btnPos = progressBarSize / (nbOfSlides-1);
				leftBtnPos = btnPos*counter;

				jQuery('.progress>span').append('<a id="' + currentPos + '" title="' + jQuery(this).data('menu-title') + '" class="slideButton"></a>');
				jQuery('a#' + currentPos).css('left',leftBtnPos);
				//var position =  jQuery('.progress').width()/(1 / Reveal.getProgress());
				currentPos++;
			})

	} 
	
	function slideButtonProgress(){
	
		var position =  jQuery('.progress').width();
		position = position * Reveal.getProgress();

		if(position === 4){
			jQuery('span.slideButtonActive').css('left', position-4 +'px');
		} else {
			jQuery('span.slideButtonActive').css('left', position +'px');
		}

	}

	Reveal.addEventListener( 'ready', function( event ) {
		jQuery(".controls").append("<span class='animnext'></span>");
		jQuery("span.animnext").hide();

		jQuery('.progress>span').append('<span class="slideButtonActive"></span>');
		var slideName = jQuery('.slides>section.present').data('slide-name');
		jQuery('button.navigate-right, button.navigate-left, a.slideButton, a.navigate-next, a.navigate-prev')
		.bind("click touchend", function(){
            slideButtonProgress();
        })

		jQuery('aside.controls').append('<span id="mobileProgress">' + (event.indexh+1) + ' / ' + jQuery(".slides > section").length + ' <span>'+ slideName +'</span></span>');
	
		Reveal.addEventListener( 'slidechanged', function( event ) {
		    // event.previousSlide, event.currentSlide, event.indexh, event.indexv
		    var slideName = jQuery('.slides>section.present').data('slide-name');
		    jQuery('span#mobileProgress').empty().append( (event.indexh+1) + ' / ' + jQuery(".slides > section").length + ' <span>'+ slideName +'</span>');
		});

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	        jQuery('body').addClass('mobile');
	 
	 		jQuery('page').prepend('<div class="playButton"></div>');
	 		jQuery('.playButton').append('<div class="tableCell"><span class="circle-bg-frame"><i class="fa fa-play" aria-hidden="true"></i></span><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>');
	        jQuery('.playButton').on('click touchend', function(e){
	            jQuery('.playButton').addClass('loading');
	            jQuery('.playButton .circle-bg-frame').hide();
	            jQuery('.fa-spin').show();

	        });
	    }

	})

	Reveal.addEventListener( 'ready', function(){
		slideProgressChapters();
        scaleContent();  

        //REFERENCES BUTTON
		jQuery('.references').css('display', 'none');

		currentSlide = 'section#' + Reveal.getCurrentSlide().id;
		Reveal.addEventListener( 'slidechanged', function( event ) {
			jQuery('.references').css('display', 'none');
			jQuery('button#references').removeClass('active');
			currentSlide = 'section#' + Reveal.getCurrentSlide().id;
		});
		jQuery('button#references').on('click',function(e){		
			jQuery(currentSlide + ' .references').toggle();
			jQuery(this).toggleClass('active');
			jQuery(".controls").toggleClass('index-position-3');
			jQuery(".progress").toggleClass('index-position-2');
			e.preventDefault();
		});
	});
    
	window.addEventListener('resize', function(){
		slideProgressChapters();
		slideButtonProgress();
	});

	Reveal.addEventListener( 'slidechanged', function( event ) {
	    // event.previousSlide, event.currentSlide, event.indexh, event.indexv
		slideButtonProgress();
	});


	function scaleContent(){
		var menuHeight = jQuery('aside.controls').height(),
			headerHeight = jQuery('.header').height(),
			footerHeight = jQuery('.footer').height() + menuHeight,
            windowHeight = document.body.clientHeight - (headerHeight + footerHeight),
			windowWidth = document.body.clientWidth,
			contentWidth = jQuery('.contentSection').width() + parseInt(jQuery('.contentSection').css("padding-left").replace("px", "")) + parseInt(jQuery('.contentSection').css("padding-right").replace("px", ""));
			contentHeight = jQuery('.contentSection').height() + parseInt(jQuery('.contentSection').css("padding-top").replace("px", "")) + parseInt(jQuery('.contentSection').css("padding-bottom").replace("px", ""));

		var scale;
		if(windowWidth < contentWidth && windowHeight > contentHeight){
			scale = windowWidth / contentWidth;
		}else{
			scale = windowHeight / contentHeight;
		}
		jQuery('.contentSection').css({
			'-moz-transform' 	: 'translateX(-50%) translateY(-50%) scale('+ scale +')',
			'-o-transform'		: 'translateX(-50%) translateY(-50%) scale('+ scale +')',
			'-webkit-transform'	: 'translateX(-50%) translateY(-50%) scale('+ scale +')',
			msTransform			: 'translateX(-50%) translateY(-50%) scale('+ scale +')',
			'transform'			: 'translateX(-50%) translateY(-50%) scale('+ scale +')',
			msFilter			: 'progid:DXImageTransform.Microsoft.Matrix(M11=1, M12=0, M21=0, M22=1, SizingMethod="auto expand")'
		})
	}

	function simpleScale(){
		var windowHeight = document.body.clientHeight,
			windowWidth = document.body.clientWidth,
			contentWidth = jQuery('.scaleContent').width() + parseInt(jQuery('.scaleContent').css("padding-left").replace("px", "")) + parseInt(jQuery('.scaleContent').css("padding-right").replace("px", ""));
			contentHeight = jQuery('.scaleContent').height() + parseInt(jQuery('.scaleContent').css("padding-top").replace("px", "")) + parseInt(jQuery('.scaleContent').css("padding-bottom").replace("px", ""));
		var elementHeight = windowWidth*contentHeight / contentWidth, 
			scale = elementHeight/contentHeight;
			console.log(contentWidth);
		jQuery('.scaleContent').css({
			'-moz-transform' 	: 'translateX(-50%) scale('+ scale +')',
			'-o-transform'		: 'translateX(-50%) scale('+ scale +')',
			'-webkit-transform'	: 'translateX(-50%) scale('+ scale +')',
			msTransform			: 'translateX(-50%) scale('+ scale +')',
			'transform'			: 'translateX(-50%) scale('+ scale +')',
			msFilter			: 'progid:DXImageTransform.Microsoft.Matrix(M11=1, M12=0, M21=0, M22=1, SizingMethod="auto expand")'
		})
	}

	window.addEventListener('resize', scaleContent);
	window.addEventListener('resize', simpleScale);

    jQuery( document ).ready(function() {
        scaleContent();
        simpleScale();
    });
    
    /** PULSING THE NEXT BUTTON **/
    document.addEventListener('playbackready', function(){
         jQuery('audio').on('ended', function(){
             startPulsing();
         });
    });
    
    Reveal.addEventListener( 'slidechanged', function( event ) {
        stopPulsing();
    });
    
    function startPulsing(){
        //jQuery('.navigate-right.enabled').addClass('infinite');
        //jQuery('.navigate-right.enabled').animateCss(animation);
        jQuery("span.animnext").show();
        TweenMax.to(jQuery("span.animnext"), 1.5, {scale:3, opacity:0, repeat:-1, ease: Power2.easeOut});
    }
    
    function stopPulsing(){
        //jQuery('.navigate-right.enabled').removeClass('infinite');
        //jQuery('.navigate-right.enabled').removeClass('shake');
        jQuery("span.animnext").hide();
    }
	
	//FEATHERLIGHT : Lightbox configuration
	jQuery.featherlight.defaults.closeIcon = '';

})

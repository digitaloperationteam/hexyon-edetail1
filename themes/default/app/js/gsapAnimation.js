(function ($) {
    
    function gsapAnimation (origin, target, duration, animationType, delayTime, staggerTime){
        if(!delayTime){ delayTime = 0; }
        if(!staggerTime){ staggerTime = 0; }
        
        var vars;
        switch(animationType) {
            case "fadeIn":
                vars = {opacity:0, delay:delayTime}
                break;
            case "fadeOut":
                vars = {opacity:1, delay:delayTime}
                break;
            case "slideFromLeft":
                vars = {opacity:0, x:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromRight":
                vars = {opacity:0, x:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromTop":
                vars = {opacity:0, y:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromMiddleTop":
                vars = {opacity:0, y:-50, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromBottom":
                vars = {opacity:0, y:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomIn":
                vars = {opacity:0, scale:0, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomInOut":
                vars = {opacity:0, scale:0, ease:Back.easeInOut.config(1.7), delay:delayTime}
                break;
            case "bounce":
                vars = {scale:0, ease: Bounce.easeOut, delay:delayTime}
                break;
            case "rotation":
                vars = {rotation:360, repeat:1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "infiniteRotation":
                vars = {rotation:360, repeat:-1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "slowWiggle":
                vars = {scale:0.95, yoyo:true, repeat:-1, ease:Bounce.easeInOut, delay:delayTime}
                break;
            case "borderWhite":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
            case "borderBlack":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
        }
        switch(origin) {
            case "from":
                TweenMax.from(target, duration, vars);
                break;
            case "staggerFrom":
                TweenMax.staggerFrom(target, duration, vars, staggerTime);
                break;
        }
        
    }
    
    $(document).ready(function () {
        var intro_0_0_0 = $('#intro-0-0-0');
            /*****ANIMATIONS*****/
    });

    
    Reveal.addEventListener( 'slidechanged', function ( event ) {
        var currentSlide = $(event.currentSlide);
        //00_INTRO
        if(currentSlide.hasClass('intro')) {
            //GENERAL
            gsapAnimation('from', currentSlide.find('h2'), 1.5, 'slideFromTop', 0.5);
            gsapAnimation('from', currentSlide.find('h3'), 1.5, 'slideFromBottom', 0.5);
            gsapAnimation('from', currentSlide.find('h4, h6, p'), 2.5, 'fadeIn', 1.5);
            
            //0-0-0
            /*************
            /!\ ON DOCUMENT READY /!\
            *************/
            
            //0-1-0
            var intro_0_1_0 = currentSlide.filter($('#intro-0-1-0'));
            
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', intro_0_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', intro_0_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', intro_0_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
            
            //0-2-0
        }
        
        //01_BURDEN
        if(currentSlide.hasClass('burden')) {
            //1-1-0
            var burden_1_1_0 = currentSlide.filter($('#burden-1-1-0'));
                /*****ANIMATIONS*****/
                    //Circles & Fill
                    gsapAnimation('from', burden_1_1_0.find('.fill'), 0.5, 'fadeIn', 1);
                    gsapAnimation('from', burden_1_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', burden_1_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', burden_1_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('staggerFrom', burden_1_1_0.find('table tr'), 1, 'fadeIn', 0.5, 0.2);
                    gsapAnimation('from', burden_1_1_0.find('.menuTab'), 1.5, 'fadeIn', 0.5);
                    gsapAnimation('from', burden_1_1_0.find('h2'), 1.5, 'slideFromTop', 0.5);
                    gsapAnimation('from', burden_1_1_0.find('h3'), 1.5, 'fadeIn', 1);
                    gsapAnimation('from', burden_1_1_0.find('.references'), 1.5, 'fadeIn', 1);
            
            
            //1-1-1 & 1-1-2 & 1-1-3 & 1-1-4 & 1-1-5
            var burden_1_1 = currentSlide.filter($('#burden-1-1-1, #burden-1-1-2, #burden-1-1-3, #burden-1-1-4, #burden-1-1-5'));
                /*****ANIMATIONS*****/
                    //Content
                    gsapAnimation('staggerFrom', burden_1_1.find('table tr'), 1, 'fadeIn', 0, 0.2);
            
            //1-1-1
            //1-1-2
            //1-1-3
            //1-1-4
            //1-1-5
            //1-2-0
            var burden_1_2_0 = currentSlide.filter($('#burden-1-2-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', burden_1_2_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', burden_1_2_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', burden_1_2_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', burden_1_2_0.find('h2'), 1.5, 'slideFromLeft', 0.5);
                    gsapAnimation('staggerFrom', burden_1_2_0.find('.content-circle'), 2, 'zoomInOut', 4.5, 6.5);
                    gsapAnimation('from', burden_1_2_0.find('.content-circle:first-child'), 1, 'fadeOut', 12);
                    gsapAnimation('from', burden_1_2_0.find('.content-circle:nth-child(2)'), 1, 'fadeOut', 18.5);
                    gsapAnimation('from', burden_1_2_0.find('.content-circle:nth-child(3)'), 1, 'fadeOut', 23);
                    gsapAnimation('staggerFrom', burden_1_2_0.find('.content-circle'), 2, 'fadeIn', 23, 0.3);
        }

        //02_NSAIDS
        if(currentSlide.hasClass('nsaids')) {
            
            //2-1-1
            var nsaids_2_1_1 = currentSlide.filter($('#nsaids-2-1-1'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_1_1.find('.fill'), 0.5, 'fadeIn', 0);
                    gsapAnimation('from', nsaids_2_1_1.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', nsaids_2_1_1.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', nsaids_2_1_1.find('.circle-bg'), 2, 'fadeIn', 1);
                    gsapAnimation('from', nsaids_2_1_1.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', nsaids_2_1_1.find('h2'), 1, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_1_1.find('h3'), 1.5, 'fadeIn', 3);
                    gsapAnimation('staggerFrom', nsaids_2_1_1.find('.absolute-content, img, li, p'), 1, 'fadeIn', 4, 0.2);
            
            //2-1-2
            var nsaids_2_1_2 = currentSlide.filter($('#nsaids-2-1-2'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_1_2.find('.circle-bg-frame'), 1.5, 'rotation', 0);
                    //Content
                    gsapAnimation('from', nsaids_2_1_2.find('h2'), 1, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_1_2.find('h3'), 1.5, 'fadeIn', 3);
                    gsapAnimation('staggerFrom', nsaids_2_1_2.find('.absolute-content, .col-right'), 1, 'fadeIn', 4, 0.2);
                    gsapAnimation('staggerFrom', nsaids_2_1_2.find('li, p'), 1, 'fadeIn', 5, 0.2);
                    
            //2-2-0
            var nsaids_2_2_0 = currentSlide.filter($('#nsaids-2-2-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_2_0.find('.bottom-circle'), 2, 'slideFromBottom', 1);
                    gsapAnimation('from', nsaids_2_2_0.find('.top-circle'), 2, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_2_0.find('.main-circle'), 2, 'slideFromRight', 2);
                    gsapAnimation('from', nsaids_2_2_0.find('.fill'), 2, 'fadeIn', 1);
                    //Content
                    gsapAnimation('from', nsaids_2_2_0.find('h2'), 1, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_2_0.find('h3'), 1.5, 'fadeIn', 3);
                    gsapAnimation('staggerFrom', nsaids_2_2_0.find('.col-left, .col-right'), 1, 'fadeIn', 4, 0.5);
            //2-3-1
            var nsaids_2_3_1 = currentSlide.filter($('#nsaids-2-3-1'));
                /*****ANIMATIONS*****/
                    //Circles
                    //Content
                    gsapAnimation('from', nsaids_2_3_1.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('from', nsaids_2_3_1.find('h3'), 1, 'fadeIn', 2);
                    gsapAnimation('staggerFrom', nsaids_2_3_1.find('.menuTab, .containerTab'), 1, 'fadeIn', 3, 0.5);
            //2-3-2
            var nsaids_2_3_2 = currentSlide.filter($('#nsaids-2-3-2'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_3_2.find('.circle-bg-frame'), 1.5, 'rotation', 0);
                    //Content
                    gsapAnimation('staggerFrom', nsaids_2_3_2.find('.containerTab'), 1, 'fadeIn', 1.5, 0.5);
            //2-3-3
            var nsaids_2_3_3 = currentSlide.filter($('#nsaids-2-3-3'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_3_3.find('.circle-bg-frame'), 1.5, 'rotation', 0);
                    //Content
                    gsapAnimation('staggerFrom', nsaids_2_3_3.find('.containerTab'), 1, 'fadeIn', 1.5, 0.5);
            //2-4-0
            var nsaids_2_4_0 = currentSlide.filter($('#nsaids-2-4-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', nsaids_2_4_0.find('.bottom-circle'), 2, 'slideFromBottom', 1);
                    gsapAnimation('from', nsaids_2_4_0.find('.top-circle'), 2, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_4_0.find('.main-circle'), 2, 'slideFromRight', 2);
                    gsapAnimation('from', nsaids_2_4_0.find('.fill'), 2, 'fadeIn', 1);
                    //Content
                    gsapAnimation('from', nsaids_2_4_0.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('staggerFrom', nsaids_2_4_0.find('.circle-bg-frame-2'), 2, 'zoomInOut', 5, 3);
            //2-5-0
            var nsaids_2_5_0 = currentSlide.filter($('#nsaids-2-5-0'));
                /*****ANIMATIONS*****/
                    gsapAnimation('from', nsaids_2_5_0.find('.bottom-circle'), 2, 'slideFromBottom', 1);
                    gsapAnimation('from', nsaids_2_5_0.find('.top-circle'), 2, 'slideFromTop', 1.5);
                    gsapAnimation('from', nsaids_2_5_0.find('.main-circle'), 2, 'slideFromRight', 2);
                    gsapAnimation('from', nsaids_2_5_0.find('.fill'), 2, 'fadeIn', 1);
                    //Content
                    gsapAnimation('from', nsaids_2_5_0.find('h2'), 1, 'slideFromTop', 0.75);
                    //Content
                    gsapAnimation('staggerFrom', nsaids_2_5_0.find('.content-circle'), 2, 'zoomInOut', 5, 2.5);
                    gsapAnimation('staggerFrom', nsaids_2_5_0.find('.content-circle'), 1, 'fadeOut', 9, 2.5);
                    gsapAnimation('staggerFrom', nsaids_2_5_0.find('.content-circle'), 1, 'fadeIn', 14, 0.5);
        }

        //03_PRODUCT
        if(currentSlide.hasClass('product')) {
            //3-1-0
            var product_3_1_0 = currentSlide.filter($('#product-3-1-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', product_3_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', product_3_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', product_3_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    gsapAnimation('from', product_3_1_0.find('.fill'), 2, 'fadeIn', 1);
                    //Content
                    gsapAnimation('from', product_3_1_0.find('h2'), 1, 'slideFromTop', 1);
                    gsapAnimation('staggerFrom', product_3_1_0.find('.content-circle'), 3, 'zoomInOut', 3, 8);
                    gsapAnimation('staggerFrom', product_3_1_0.find('.content-circle'), 1, 'fadeOut', 11, 8);
                    gsapAnimation('staggerFrom', product_3_1_0.find('.content-circle'), 0.5, 'fadeIn', 50, 0.5);
                    gsapAnimation('from', product_3_1_0.find('.references'), 1, 'fadeIn', 1.5);
            //3-2-0
            //3-3-0
            var product_3_3_0 = currentSlide.filter($('#product-3-3-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', product_3_3_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', product_3_3_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', product_3_3_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', product_3_3_0.find('h2'), 1, 'slideFromTop', 1.5);
                    gsapAnimation('from', product_3_3_0.find('h3'), 1.5, 'fadeIn', 3);
                    gsapAnimation('staggerFrom', product_3_3_0.find('table tr'), 1, 'fadeIn', 3, 0.2);
                    gsapAnimation('from', product_3_3_0.find('table td:first-child'), 1.5, 'borderWhite', 3.5);

        }

        //04_EFFICACY
        if(currentSlide.hasClass('efficacy')) {
            gsapAnimation('from', currentSlide.find('.safetyCircle'), 1, 'slideFromBottom', 3.5);
            
            //4-X-0
            var efficacy_4_x_0 = currentSlide.filter($('#efficacy-4-1-0, #efficacy-4-2-0, #efficacy-4-3-0, #efficacy-4-4-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', efficacy_4_x_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', efficacy_4_x_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', efficacy_4_x_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', efficacy_4_x_0.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('from', efficacy_4_x_0.find('h3'), 1, 'slideFromMiddleTop', 1.5);
                    gsapAnimation('staggerFrom', efficacy_4_x_0.find('.left, .center, .right'), 1.5, 'slideFromBottom', 2, 0);
                    gsapAnimation('from', efficacy_4_x_0.find('.references'), 1, 'fadeIn', 3);
            //4-X-1
            var efficacy_4_x_1 = currentSlide.filter($('#efficacy-4-1-1, #efficacy-4-2-1, #efficacy-4-3-1, #efficacy-4-4-1'));
                /*****ANIMATIONS*****/
                    //Circles
                    //Content
                    
                    gsapAnimation('from', efficacy_4_x_1.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('from', efficacy_4_x_1.find('h3'), 1, 'slideFromMiddleTop', 1.5);
                    gsapAnimation('staggerFrom', efficacy_4_x_1.find('ul li'), 1.5, 'slideFromLeft', 1.5, 0.4);
                    gsapAnimation('staggerFrom', efficacy_4_x_1.find('.right, .references'), 1, 'fadeIn', 3.5, 0.2);
            
            //4-X-2
            var efficacy_4_x_2 = currentSlide.filter($('#efficacy-4-1-2, #efficacy-4-2-2, #efficacy-4-3-2, #efficacy-4-4-2'));
                /*****ANIMATIONS*****/
                    //Circles
                    //Content
                    gsapAnimation('from', efficacy_4_x_2.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('from', efficacy_4_x_2.find('.content-0-2 p:first-child'), 1, 'slideFromMiddleTop', 2);
                    gsapAnimation('staggerFrom', efficacy_4_x_2.find('.content-0-2 p:not(.content-0-2 p:first-child)'), 1, 'fadeIn', 2.7, 0.7);
            //4-5-1
        }
        //05_SIDE EFFECTS
        if(currentSlide.hasClass('side-effects')) {
            gsapAnimation('from', currentSlide.find('.safetyCircle'), 1, 'slideFromBottom', 4);
            
            //5-1-0
            var side_effects_5_1_0 = currentSlide.filter($('#side-effects-5-1-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', side_effects_5_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', side_effects_5_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', side_effects_5_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', side_effects_5_1_0.find('h2'), 1, 'slideFromTop', 0.5);
                    gsapAnimation('from', side_effects_5_1_0.find('.emph.absolute-content'), 1, 'fadeIn', 2);
                    gsapAnimation('staggerFrom', side_effects_5_1_0.find('table tr'), 1.5, 'slideFromLeft', 2.5, 0.4);
                    gsapAnimation('from', side_effects_5_1_0.find('table td:nth-child(2), table td:nth-child(3)'), 1.5, 'borderBlack', 5);
                    gsapAnimation('from', side_effects_5_1_0.find('.bottom-content'), 1.5, 'fadeIn', 6.5);
                    gsapAnimation('from', side_effects_5_1_0.find('.medal'), 1, 'bounce', 1);
            //5-X-1
            var side_effects_5_x_1 = currentSlide.filter($('#side-effects-5-2-1, #side-effects-5-3-1, #side-effects-5-4-1, #side-effects-5-5-1'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', side_effects_5_x_1.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', side_effects_5_x_1.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', side_effects_5_x_1.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', side_effects_5_x_1.find('h2'), 1, 'slideFromTop', 0.5);
                    gsapAnimation('staggerFrom', side_effects_5_x_1.find('p.top, h4, .graph, .bottom-content'), 1, 'slideFromBottom', 1.5, 0.5);
                    gsapAnimation('from', side_effects_5_x_1.find('.medal'), 1, 'bounce', 1);
            
            //5-X-2
            var side_effects_5_x_2 = currentSlide.filter($('#side-effects-5-2-2, #side-effects-5-3-2, #side-effects-5-4-2, #side-effects-5-5-2'));
                /*****ANIMATIONS*****/
                    //Content
                    gsapAnimation('from', side_effects_5_x_2.find('h2'), 1, 'slideFromTop', 0.5);
                    gsapAnimation('from', side_effects_5_x_2.find('p.top'), 1, 'fadeIn', 1.5);
                    gsapAnimation('staggerFrom', side_effects_5_x_2.find('ul li'), 1, 'slideFromLeft', 2, 0.5);
                    gsapAnimation('from', side_effects_5_x_2.find('.bottom-content'), 1, 'fadeIn', 4);
                    gsapAnimation('from', side_effects_5_x_2.find('.medal'), 1, 'bounce', 1);
            
            //5-6-X
            var side_effects_5_6_x = currentSlide.filter($('#side-effects-5-6-1, #side-effects-5-6-2'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', side_effects_5_6_x.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', side_effects_5_6_x.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', side_effects_5_6_x.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', side_effects_5_6_x.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('staggerFrom', side_effects_5_6_x.find('h3, h6, p'), 1.5, 'fadeIn', 1, 0.4);
        }
        
        //06_CONCLUSION
        if(currentSlide.hasClass('conclusion')) {
            //6-1-0
            var conclusion_6_1_0 = currentSlide.filter($('#conclusion-6-1-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', conclusion_6_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', conclusion_6_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', conclusion_6_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    gsapAnimation('from', conclusion_6_1_0.find('.fill'), 2, 'fadeIn', 1.5);
                    //Content
                    gsapAnimation('from', conclusion_6_1_0.find('h2'), 1, 'slideFromTop', 0.75);
                    gsapAnimation('staggerFrom', conclusion_6_1_0.find('.content-circle'), 3, 'zoomInOut', 3, 10);
                    gsapAnimation('staggerFrom', conclusion_6_1_0.find('.content-circle'), 1, 'fadeOut', 11, 10);
                    gsapAnimation('staggerFrom', conclusion_6_1_0.find('.content-circle'), 0.5, 'fadeIn', 45, 0.5);
        }

        //07_SAFETY
        if(currentSlide.hasClass('safety')) {
            //7-1-0
            //7-2-0
            var safety_7_2_0 = currentSlide.filter($('#safety-7-2-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', safety_7_2_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', safety_7_2_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', safety_7_2_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    //Content
                    gsapAnimation('from', safety_7_2_0.find('h2'), 1, 'slideFromTop', 1);
                    gsapAnimation('from', safety_7_2_0.find('ul'), 1, 'slideFromBottom', 1.5);
        }
        
        //08_REFERENCES
        if(currentSlide.hasClass('references')) {
            //8-1-0
            var references_8_1_0 = currentSlide.filter($('#references-8-1-0'));
                /*****ANIMATIONS*****/
                    //Circles
                    gsapAnimation('from', references_8_1_0.find('.bottom-circle'), 2, 'slideFromBottom', 0);
                    gsapAnimation('from', references_8_1_0.find('.top-circle'), 2, 'slideFromTop', 0.5);
                    gsapAnimation('from', references_8_1_0.find('.main-circle'), 2, 'slideFromRight', 1);
                    gsapAnimation('from', references_8_1_0.find('.fill'), 2, 'fadeIn', 1.5);
                    //Content
                    gsapAnimation('from', references_8_1_0.find('h2'), 1, 'slideFromTop', 0);
                    gsapAnimation('staggerFrom', references_8_1_0.find('ol li'), 0.5, 'fadeIn', 1, 0.3);
        
            //8-1-1
            var references_8_1_x = currentSlide.filter($('#references-8-1-1, #references-8-1-2'));
                /*****ANIMATIONS*****/
                     gsapAnimation('from', references_8_1_x.find('.fill'), 2, 'fadeIn', 1.5);
                    //Content
                    gsapAnimation('from', references_8_1_x.find('h2'), 1, 'slideFromTop', 0);
                    gsapAnimation('staggerFrom', references_8_1_x.find('ol li, .content, img'), 0.5, 'fadeIn', 1, 0.3);
            //8-1-2
        }            
        
    });
    
})(jQuery);
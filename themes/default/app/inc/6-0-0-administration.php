<section id="administration-6-0-0" class="administration" data-background="images/bg-presentation.jpg" data-slide-name="Schéma d'administration" data-mainsection-title="Schéma d'administration" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s011" data-screen-label="Schéma d'administration" data-screen-name="Schéma d'administration" data-screen-section="" data-menu-title="Schéma d'administration">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Hexyon<sup>®</sup> est recommandé <sup>1</sup><br>et remboursé à 65%</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
                    <h3 class="text-center">Utilisation selon le calendrier vaccinal français <sup>1</sup> à 2, 4 et 11 mois</h3>
                    <img src="images/administration-hexyon.png" class="responsive" alt="Schéma d'administration" width="924" height="198">
                    
                    <div class="column">
                        <div class="col-left">
                            <div class="whiteRadiusBox">
                                <h3 class="text-center">Co-administration possible avec vaccin pneumococcique conjugué <sup>2</sup></h3>
                                <p class="text-center">En 2 sites d’injection distincts</p>
                            </div>
                        </div>
                        <div class="col-right">
                            <div class="whiteRadiusBox">
                                <h3 class="text-center">Utilisation possible en dose de rappel <sup>2</sup></h3>
                                <p class="text-center">Quel que soit le vaccin reçu en primovaccination</p>
                                <a href="#" class="blueBtn" data-featherlight="#utilisation">En savoir plus</a>
                            </div>
                        </div>
                        
                    </div>

                    <div id="utilisation" class="feather">
                        <h2 class="uppercase">Utilisation possible en dose de rappel<sup>1</sup></h2>
                        <p class="big">Quelque soit le vaccin reçu en primovaccination :</p>
                        <div class="column">
                            <div class="col-left">
                                <h3 class="uppercase normal">PRIMOVACCINATION (2,4 mois)</h3>
                            </div>
                            <div class="col-right">
                                <h3 class="uppercase normal">Rappel (11 mois)</h3>
                            </div>
                        </div>
                        <img src="images/hexyon-utilisation.jpg" class="responsive" alt="Utilisation d'Hexyon" width="892" height="139">
                        <p class="legend">1. Calendrier vaccinal 2016 :<br><a target="_blank" href="http://social-sante.gouv.fr/IMG/pdf/calendrier_vaccinal_2016.pdf">http://social-sante.gouv.fr/IMG/pdf/calendrier_vaccinal_2016.pdf (accédé le 21/07/2016)</a></p>  
                    </div>

                    <div id="methodo" class="feather">
                        <h2 class="uppercase">CONTRE-INDICATIONS<sup>1</sup></h2>
                        <ul>
                           <li>Antécédent de réaction anaphylactique après une précédente administration d’Hexyon.</li>
                           <li>Hypersensibilité aux substances actives, à l’un des excipients, à des résidus à l'état de traces (glutaraldéhyde, formaldéhyde, néomycine, streptomycine et polymyxine B), à un vaccin coquelucheux, ou hypersensibilité suite à une précédente administration d’Hexyon ou d'un vaccin contenant les mêmes composants. </li>
                           <li>Personnes ayant présenté une encéphalopathie d’étiologie inconnue, survenue dans les 7 jours suivant l’administration d’un vaccin coquelucheux (vaccins coquelucheux à germes entiers ou acellulaires). Dans ce cas, la vaccination contre la coqueluche doit être interrompue et le schéma de vaccination doit être poursuivi avec des vaccins diphtérie, tétanos, hépatite B, poliomyélite et Hib.</li>
                           <li>Sujets présentant des troubles neurologiques non contrôlés ou une épilepsie non contrôlée avant qu’un traitement n’ait été mis en place, que l'état du patient n’ait été stabilisé et que le bénéfice ne soit clairement supérieur au risque.</li>
                        </ul>
                        <p class="legend">1. Résumé des Caractéristiques du Produit.</p> 
                    </div>
                    
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Avis du Haut Conseil de la Santé Publique du 20 février 2015 relatif à l'utilisation du vaccin hexavalent HEXYON® destiné à la primo-vaccination et à la vaccination du rappel des nourrissons contre la diphtérie, le tétanos, la coqueluche, l'hépatite B, la poliomyélite et les infections invasives à <i>Haemophilus influenzae</i> type b.</li>
                        <li class="no-bullet">2. Résumé des Caractéristiques du Produit.</li>
					</ul>
				</div>
                <button id="contreIndications" class="footerBtn" data-featherlight="#methodo">Contre-indications</button>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
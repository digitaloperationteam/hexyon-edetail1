<section id="intro-0-0-0" class="accueil" data-background="images/bg-intro.jpg" data-mainsection-title="Accueil" data-slide-name="Accueil" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s001" data-screen-label="Accueil" data-screen-name="accueil" data-screen-section="" data-menu-title="Accueil">
   <div class="scalableContainer">
        <div class="contentSection">
            <div class="mainContent">
            	
            </div>

        </div>
    </div>
    <div class="macaron absolute-content">
		<img src="images/macaron.png" alt="Prêt à l'emploi | 6 en 1">
	</div>
    <div class="footer">
    	<div class="columns">
			<div class="cell">
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
			</div>
			<div class="cell">
				<p class="text-left"><span class="bold">Indications :</span> Hexyon® (D-T-Polio-Ca-Hib-HepB) est indiqué chez le nourrisson, à partir de l’âge de 6 semaines pour la primovaccination et la vaccination de rappel contre la diphtérie, le tétanos, la coqueluche, l’hépatite B, la poliomyélite et les maladies invasives à <i>Haemophilus influenzae</i> type b (Hib). L’utilisation de ce vaccin doit se faire conformément aux recommandations officielles.<br>* Ligne de production d'Hexyon sur le site de Sanofi Pasteur à Marcy l'Etoile près de Lyon.</p>
				<p class="text-center">FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
			</div>
			<div class="cell">
				<img src="images/spmsd-logo.png" alt="Sanofi Pasteur MSD">
			</div>
		</div>
	</div>
    
</section>
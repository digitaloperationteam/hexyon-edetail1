<section id="tolerance-3-0-0" class="tolerance international" data-background="images/bg-tolerance.jpg" data-mainsection-title="2. Tolérance & Expérience" data-slide-name="Tolérance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s004" data-screen-label="Une expérience internationale significative" data-screen-name="Tolérance" data-screen-section="Expérience internationale" data-menu-title="Tolérance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Une expérience internationale<br>significative</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2 class="text-left">Distribué en Europe et dans le monde, dans plus de 35 pays.<sup>1</sup></h2>
            		<p class="text-left">Dont l’Allemagne, l’Italie, la Belgique, la Grèce et l’Espagne.<br>Chez nos voisins allemands depuis Juillet 2013<sup>1</sup> : > 1 400 000 doses distribuées</p>
            		<div class="whiteBubble">
            			<p><span class='bold'>Plus de</span><br><span class='bold'>15 millions</span><br>de doses distribuées dans le monde<sup>1</sup></p>
            		</div>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Données internes SPMSD</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
<section id="tolerance-3-2-0" class="tolerance" data-background="images/bg-presentation.jpg" data-slide-name="Tolérance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s006" data-screen-label="Profil de tolérance - Effets indésirables - 1" data-screen-name="Tolérance" data-screen-section="Effets indésirables - 1" data-menu-title="Tolérance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>un bon profil de tolérance</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2>Effets indésirables les plus fréquemment rapportés<br> dans les essais cliniques et après la mise sur le marché<sup>1</sup> :</h2>
            		<div class="column">
                        <div class="col-left">
                            <div class="whiteRadiusBox">
                                <h3>Systémiques</h3>
                                <ul>
                                    <li>Anorexie (diminution de l'appétit), pleurs, somnolence, vomissements, irritabilité, fièvre ≥ 38,0°C</li>
                                    <li>Pleurs anormaux (prolongés) et Diarrhée</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-right">
                            <div class="whiteRadiusBox">
                                <h3>Au site d’injection</h3>
                                <ul>
                                    <li>Douleur, érythème et œdème</li>
                                    <li>Induration</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <p>Une réactogénicité sollicitée légèrement supérieure a été observée après la première dose, par rapport aux doses suivantes.<br><span class="medium">Pour plus d'information sur les effets indésirables peu fréquents ou rares, consultez le Résumé des Caractéristiques du Produit.</span></p>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Résumé des Caractéristiques du Produit.</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
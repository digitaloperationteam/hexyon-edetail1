<section id="immunogenicite-2-0-0" class="immunogenicite" data-background="images/bg-presentation.jpg" data-mainsection-title="1. Immunogenicité" data-slide-name="Immunogenicité" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s003" data-screen-label="Une réponse immunitaire élevée" data-screen-name="Immunogenicité" data-screen-section="" data-menu-title="Immunogenicité">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Une Réponse immunitaire élevée<br>pour tous les antigènes</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2>Non infériorité d’Hexyon<sup>®</sup> versus Infanrix<sup>®</sup> Hexa démontrée</h2>
            		<p class="text-center big">après la dose de rappel pour tous les antigènes avec un schéma<br>vaccinal 2+1* et en co-administration avec Prevenar 13<sup>®1</sup></p>
            		<div class="column">
            			<div class="col-left">
            				<p class="imageTitle">Taux de séroprotection/séroconversion 1 mois après dose de rappel</p>
            				<img src="images/graph01.png" alt="Graphique" width="724" height="280">
            				<p class="legend">* 2 doses de primovaccination suivies d'1 dose de rappel à 3, 5 et 11/12 mois.</p>
            			</div>
            			<div class="col-right">
            				<button class="greyBtn" data-featherlight="#methodologieEtude">Méthodologie de l'étude</button>
            				<button class="greyBtn" data-featherlight="#donneesToleranceEtude">Données de tolérance<br> de l'étude</button>
            			</div>
            		</div>
            		<div id="methodologieEtude" class="feather">
            			<h2>MÉTHODOLOGIE DE L'ÉTUDE</h2>
            			<div class="scroll">
							<p>Essai de phase III, multicentrique, randomisé, comparatif, avec évaluation en insu.<br>
							Vaccination à 3 mois, 5 mois et 11/12 mois<br>
							Vaccins co-administrés avec Prevenar 13<sup>®</sup></p>
							<h3>Critères principaux de jugement de l’étude</h3>
							<p class="bold blue">Seuils de séroprotection/séroconversion 1 mois après la dose de rappel dans la population Per Protocole (Hexyon<sup>®</sup> n=249, Infanrix<sup>®</sup> Hexa n=248) :</p>
							<ul class="no-bullets">
								<li class="no-bullet">Anti-D/T : ≥ 0,1 UI/mL</li>
								<li class="no-bullet">Anti-PT/FHA : Réponse vaccinale*</li>
								<li class="no-bullet">Anti-Polio : ≥ 8 1/dil</li>
								<li class="no-bullet">Anti-HBs : ≥ 10 mUI/mL</li>
								<li class="no-bullet">Anti-PRP : ≥ 1 μg/mL</li>
							</ul>
							<p>* Anti-PT/FHA ≥ 8 unités Elisa (UE)/mL chez les nourrisons avec des concentrations initiales (avant 1ère administration) &lt;8 UE/mL<br>Anti-PT/FHA ≥ concentrations initiales, si concentrations initiales d’Ac ≥ 8 UE/mL</p>	
						</div>
            		</div>
            		<div id="donneesToleranceEtude" class="feather">
            			<h2>Résultats de Tolérance évalués dans cette étude <sup>1</sup> :</h2>
            			<div class="scroll">
							<ul>
								<li>Les 2 vaccins ont démontré un bon profil de tolérance.<sup>1</sup></li>
								<li>Le profil de tolérance était similaire à Infanrix<sup>®</sup> Hexa bien que le pourcentage de sujets ayant présenté une douleur au site d’injection et de la fièvre ait été supérieur pour Hexyon<sup>®</sup> dans les 7 jours suivant la vaccination.<sup>1</sup></li>
							</ul>
							<h3 class="dark-blue">Réactions au site d’injection</h3>
							<img src="images/immunite_tab2.jpg" class="responsive" alt="Tableau des réactions au site d’injection" width="859" height="306">

							<p class="legend">
								1. Rapport d’étude A3L38. EudraCT #2012-001054-26<br>
								* Tous les sujets ont reçu une dose de Prevenar 13<sup>®</sup>, simultanément au vaccin de l’étude.<br>
								† Réactions au site d’injection Hexyon® ou Infanrix<sup>®</sup> Hexa.
							</p>

							<h3 class="dark-blue">Réactions au site d’injection</h3>
							<img src="images/immunite_tab3.jpg" class="responsive" alt="Tableau des réactions au site d’injection" width="867" height="446">

							<p class="legend">
								1. Rapport d’étude A3L38. EudraCT #2012-001054-26<br>
								* Réactions pouvant être rapportées pour chacun des vaccins (Hexyon<sup>®</sup>, Infanrix<sup>®</sup> Hexa ou Prevenar 13<sup>®</sup>).
							</p>
						</div>

            		</div>
            		<div id="popupTolerance" class="feather">
            			<h2>Un bon profil de tolérance</h2>
            			<div class="scroll">
							<h3>Analyse intégrée de la tolérance des essais de primovaccination comparatifs à Infanrix<sup>®</sup> Hexa Réactions « sollicitées » après primovaccination - Population de Tolérance</h3>
							<img src="images/immunite_tab1.jpg" class="responsive" alt="Tableau d'analyse" width="895" height="402">
							<p>Analyse intégrée des essais comparatifs à Infanrix Hexa (A3L11, A3L12, A3L17, A3L24, A3L38)<br>Population de Tolérance<br>
							Réactions pouvant être rapportées pour chacun des vaccins (Hexavalent ou Prevenar/Prevenar 13 en cas de co-administration)</p>

							<h3>Effets indésirables les plus fréquemment rapportés dans les essais cliniques et après la mise sur le marché<sup>1</sup> :</h3>
							<h3 class="dark-blue">Systémiques :</h3>
							<ul>
								<li>Anorexie (diminution de l'appétit), pleurs, somnolence, vomissements, irritabilité, fièvre ≥ 38,0°C</li>
								<li>Pleurs anormaux (prolongés) et Diarrhée</li>
							</ul>
							<h3 class="dark-blue">Au site d'injection :</h3>
							<ul>
								<li>Douleur, érythème et œdème</li>
								<li>Induration</li>
							</ul>
							<p>Une réactogénicité sollicitée légèrement supérieure a été observée après la première dose, par rapport aux doses suivantes.</p>
							<p>Pour plus d'information sur les effets indésirables peu fréquents ou rares, consultez le Résumé des Caractéristiques du Produit.</p>
							<p class="legend">1. Résumé des Caractéristiques du Produit.</p>

							<h3>Surveillance du profil de tolérance</h3>
							<p>&#9660; Comme tout médicament contenant une nouvelle substance active autorisée dans l’Union Européenne, Hexyon fait l'objet  d’une surveillance supplémentaire qui permettra l’identification rapide de nouvelles informations relatives à la sécurité.</p>
							<h3>Plan de Gestion des Risques</h3>
							<h3 class="dark-blue">Les événements sous surveillance pour des raisons historiques ou Dits d'effet de classe sont :</h3>
							<ul>
								<li>Apnée</li>
								<li>Encéphalopathie, encéphalite / encéphalomyélite aiguë disséminée</li>
							</ul>
							<h3 class="dark-blue">Sont également surveillés les risques dits importants :</h3>
							<ul>
								<li>Œdème étendu du membre</li>
								<li>Episode d'hypotonie-hyporéactivité</li>
								<li>Convulsions avec ou sans fièvre</li>
								<li>Réaction anaphylactique</li>
							</ul>
						</div>

            		</div>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Rapport d'étude A3L38. EudraCT #2012-001054-26.</li>
					</ul>
				</div>
				<!--<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160829_Tolerance.pdf?Expires=1493798718&Signature=Dta6so5NIhkr33kwjAVhnLHrnFazubyiEbywJsH0qyPdOuUTRZKwjytveMjymGKQiaNohvZKyIq4fFyeZGUwIKa2mePA3w8c638TgFXrifQAeJLWQo8IQv9MtFkAJTlL-9oJdJZ2-FJANBWmu9BpBs7GtnHTf~wwpPUsfCJe2yE2rQvMH-b2vqm7SoMwrJVsmDTPhpF3bmxPixgDK89dzeZMYX-bCZIMZ7QO1kZiAaVciy32QOaUpnJX3gclPzz27Vnszg72~pn~kjO0hcej3vIEsQTGqUcgMTF9uiibDl9njiVfB5O7odApphRFCYCJewEf0lw0iK7C7D6zrJqF2A__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="tolerance" class="footerBtn">Tolérance</a>-->
				<button id="tolerance" class="footerBtn" data-featherlight="#popupTolerance">Tolérance</button>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
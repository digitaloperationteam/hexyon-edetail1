<section id="tolerance-3-3-0" class="tolerance" data-background="images/bg-presentation.jpg" data-slide-name="Tolérance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s007" data-screen-label="Profil de tolérance - Effets indésirables - 2" data-screen-name="Tolérance" data-screen-section="Effets indésirables - 2" data-menu-title="Tolérance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>un bon profil de tolérance</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<p class="big text-left">&#9660; Comme tout médicament contenant une nouvelle substance active autorisée dans l’Union Européenne, Hexyon fait l'objet d’une surveillance supplémentaire qui permettra l’identification rapide de nouvelles informations relatives à la sécurité.</p>
                    <h2 class="text-left">Plan de Gestion des Risques</h2>
                    <h3 class="text-left">Les évènements sous surveillance pour des raisons historiques ou dits d'effet de classe sont :</h3>
                    <ul>
                        <li>Apnée</li>
                        <li>Encéphalopathie, encéphalite / encéphalomyélite aiguë disséminée</li>
                    </ul>
                    <h3 class="text-left">Sont également surveillés les risques dits importants :</h3>
                    <ul>
                        <li>Œdème étendu du membre</li>
                        <li>Episode d'hypotonie-hyporéactivité</li>
                        <li>Convulsions avec ou sans fièvre</li>
                        <li>Réaction anaphylactique</li>
                    </ul>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">Références</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Risk Management Plan V 10.0 (Plan de gestion des risques).</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions légales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
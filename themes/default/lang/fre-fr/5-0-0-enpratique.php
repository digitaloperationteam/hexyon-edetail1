﻿<section id="enpratique-5-0-0" class="enpratique" data-background="images/bg-presentation.jpg" data-slide-name="En pratique" data-mainsection-title="En pratique" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s010" data-screen-label="En pratique : 10 &eacute;tapes de pr&eacute;paration en moins" data-screen-name="En pratique" data-screen-section="&Eacute;tapes" data-menu-title="En pratique">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>en pratique :<br>10 &eacute;tapes de pr&eacute;paration en moins</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
                    <h3 class="bold">Vaccin &agrave; reconstituer<sup>1</sup>, 15 &eacute;tapes avant administration :</h3>
                    <img src="images/preparation-vaccin.png" class="responsive" alt="Pr&eacute;paration du vaccin" width="973" height="222">
                    <h3 class="bold">Hexyon, 5 &eacute;tapes avant administration<sup>2</sup> :</h3>
                    
                    
                    <div class="column">
                        <div class="col-left">
                            <img src="images/preparation-hexyon.png" class="responsive" alt="Pr&eacute;paration d'Hexyon" width="813" height="168">
                        </div>
                        <div class="col-right">
                            <a href="#" data-featherlight="#video"><img src="images/video.png" class="responsive" alt="Voir la vid&eacute;o" width="158" height="176"></a>
                        </div>
                    </div>
                    <div id="conservation" class="feather">
                        <h2 class="uppercase">Conservation<sup>1</sup></h2>
                        <h3 class="uppercase">Dur&eacute;e : 3 ans</h3>
                        <h3 class="uppercase">Pr&eacute;cautions particuli&egrave;res :</h3>
                        <ul>
                           <li>A conserver au r&eacute;frig&eacute;rateur (entre 2&deg;C et 8&deg;C).</li>
                           <li>Ne pas congeler.</li>
                           <li>Conserver le conditionnement primaire dans l&rsquo;emballage ext&eacute;rieur, &agrave; l&rsquo;abri de la lumi&egrave;re.</li>
                        </ul>
                        <p class="legend">1. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</p>  
                    </div>
                    <div id="video" class="feather">
                        <iframe src="//players.brightcove.net/4931690864001/rJSbLDiJe_default/index.html?videoId=5196547525001" allowfullscreen webkitallowfullscreen mozallowfullscreen width="800" height="450"></iframe>
                    </div>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit de Pentavac.</li>
                        <li class="no-bullet">2. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit d&rsquo;Hexyon.</li>
					</ul>
				</div>
                <button id="conservation" class="footerBtn" data-featherlight="#conservation">Conservation</button>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
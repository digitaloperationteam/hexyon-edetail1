﻿<section id="tolerance-3-3-0" class="tolerance" data-background="images/bg-presentation.jpg" data-slide-name="Tol&eacute;rance" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s007" data-screen-label="Profil de tol&eacute;rance - Effets ind&eacute;sirables - 2" data-screen-name="Tol&eacute;rance" data-screen-section="Effets ind&eacute;sirables - 2" data-menu-title="Tol&eacute;rance">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>un bon profil de tol&eacute;rance</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<p class="big text-left">▼ Comme tout m&eacute;dicament contenant une nouvelle substance active autoris&eacute;e dans l&rsquo;Union Europ&eacute;enne, Hexyon fait l'objet d&rsquo;une surveillance suppl&eacute;mentaire qui permettra l&rsquo;identification rapide de nouvelles informations relatives &agrave; la s&eacute;curit&eacute;.</p>
                    <h2 class="text-left">Plan de Gestion des Risques</h2>
                    <h3 class="text-left">Les &eacute;v&egrave;nements sous surveillance pour des raisons historiques ou dits d'effet de classe sont :</h3>
                    <ul>
                        <li>Apn&eacute;e</li>
                        <li>Enc&eacute;phalopathie, enc&eacute;phalite / enc&eacute;phalomy&eacute;lite aigu&euml; diss&eacute;min&eacute;e</li>
                    </ul>
                    <h3 class="text-left">Sont &eacute;galement surveill&eacute;s les risques dits importants :</h3>
                    <ul>
                        <li>&OElig;d&egrave;me &eacute;tendu du membre</li>
                        <li>Episode d'hypotonie-hypor&eacute;activit&eacute;</li>
                        <li>Convulsions avec ou sans fi&egrave;vre</li>
                        <li>R&eacute;action anaphylactique</li>
                    </ul>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Risk Management Plan V 10.0 (Plan de gestion des risques).</li>
					</ul>
				</div>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>
﻿<section id="immunogenicite-2-0-0" class="immunogenicite" data-background="images/bg-presentation.jpg" data-mainsection-title="1. Immunogenicit&eacute;" data-slide-name="Immunogenicit&eacute;" data-locale="fre-fr" data-screen-id="2016_HEX_ed1_s003" data-screen-label="Une r&eacute;ponse immunitaire &eacute;lev&eacute;e" data-screen-name="Immunogenicit&eacute;" data-screen-section="" data-menu-title="Immunogenicit&eacute;">
   <div class="scalableContainer">
   		<div class="header">
    		<h1>Une R&eacute;ponse immunitaire &eacute;lev&eacute;e<br>pour tous les antig&egrave;nes</h1>
    	</div>
        <div class="contentSection">
            <div class="mainContent">
            	<div class="content">
            		<h2>Non inf&eacute;riorit&eacute; d&rsquo;Hexyon<sup>&reg;</sup> versus Infanrix<sup>&reg;</sup> Hexa d&eacute;montr&eacute;e</h2>
            		<p class="text-center big">apr&egrave;s la dose de rappel pour tous les antig&egrave;nes avec un sch&eacute;ma<br>vaccinal 2+1* et en co-administration avec Prevenar 13<sup>&reg;1</sup></p>
            		<div class="column">
            			<div class="col-left">
            				<p class="imageTitle">Taux de s&eacute;roprotection/s&eacute;roconversion 1 mois apr&egrave;s dose de rappel</p>
            				<img src="images/graph01.png" alt="Graphique" width="724" height="280">
            				<p class="legend">* 2 doses de primovaccination suivies d'1 dose de rappel &agrave; 3, 5 et 11/12 mois.</p>
            			</div>
            			<div class="col-right">
            				<button class="greyBtn" data-featherlight="#methodologieEtude">M&eacute;thodologie de l'&eacute;tude</button>
            				<button class="greyBtn" data-featherlight="#donneesToleranceEtude">Donn&eacute;es de tol&eacute;rance<br> de l'&eacute;tude</button>
            			</div>
            		</div>
            		<div id="methodologieEtude" class="feather">
            			<h2>M&Eacute;THODOLOGIE DE L'&Eacute;TUDE</h2>
            			<div class="scroll">
							<p>Essai de phase III, multicentrique, randomis&eacute;, comparatif, avec &eacute;valuation en insu.<br>
							Vaccination &agrave; 3 mois, 5 mois et 11/12 mois<br>
							Vaccins co-administr&eacute;s avec Prevenar 13<sup>&reg;</sup></p>
							<h3>Crit&egrave;res principaux de jugement de l&rsquo;&eacute;tude</h3>
							<p class="bold blue">Seuils de s&eacute;roprotection/s&eacute;roconversion 1 mois apr&egrave;s la dose de rappel dans la population Per Protocole (Hexyon<sup>&reg;</sup> n=249, Infanrix<sup>&reg;</sup> Hexa n=248) :</p>
							<ul class="no-bullets">
								<li class="no-bullet">Anti-D/T : &ge; 0,1 UI/mL</li>
								<li class="no-bullet">Anti-PT/FHA : R&eacute;ponse vaccinale*</li>
								<li class="no-bullet">Anti-Polio : &ge; 8 1/dil</li>
								<li class="no-bullet">Anti-HBs : &ge; 10 mUI/mL</li>
								<li class="no-bullet">Anti-PRP : &ge; 1 &mu;g/mL</li>
							</ul>
							<p>* Anti-PT/FHA &ge; 8 unit&eacute;s Elisa (UE)/mL chez les nourrisons avec des concentrations initiales (avant 1&egrave;re administration) <8 UE/mL<br>Anti-PT/FHA &ge; concentrations initiales, si concentrations initiales d&rsquo;Ac &ge; 8 UE/mL</p>	
						</div>
            		</div>
            		<div id="donneesToleranceEtude" class="feather">
            			<h2>R&eacute;sultats de Tol&eacute;rance &eacute;valu&eacute;s dans cette &eacute;tude <sup>1</sup> :</h2>
            			<div class="scroll">
							<ul>
								<li>Les 2 vaccins ont d&eacute;montr&eacute; un bon profil de tol&eacute;rance.<sup>1</sup></li>
								<li>Le profil de tol&eacute;rance &eacute;tait similaire &agrave; Infanrix<sup>&reg;</sup> Hexa bien que le pourcentage de sujets ayant pr&eacute;sent&eacute; une douleur au site d&rsquo;injection et de la fi&egrave;vre ait &eacute;t&eacute; sup&eacute;rieur pour Hexyon<sup>&reg;</sup> dans les 7 jours suivant la vaccination.<sup>1</sup></li>
							</ul>
							<h3 class="dark-blue">R&eacute;actions au site d&rsquo;injection</h3>
							<img src="images/immunite_tab2.jpg" class="responsive" alt="Tableau des r&eacute;actions au site d&rsquo;injection" width="859" height="306">

							<p class="legend">
								1. Rapport d&rsquo;&eacute;tude A3L38. EudraCT #2012-001054-26<br>
								* Tous les sujets ont re&ccedil;u une dose de Prevenar 13<sup>&reg;</sup>, simultan&eacute;ment au vaccin de l&rsquo;&eacute;tude.<br>
								&dagger; R&eacute;actions au site d&rsquo;injection Hexyon&reg; ou Infanrix<sup>&reg;</sup> Hexa.
							</p>

							<h3 class="dark-blue">R&eacute;actions au site d&rsquo;injection</h3>
							<img src="images/immunite_tab3.jpg" class="responsive" alt="Tableau des r&eacute;actions au site d&rsquo;injection" width="867" height="446">

							<p class="legend">
								1. Rapport d&rsquo;&eacute;tude A3L38. EudraCT #2012-001054-26<br>
								* R&eacute;actions pouvant &ecirc;tre rapport&eacute;es pour chacun des vaccins (Hexyon<sup>&reg;</sup>, Infanrix<sup>&reg;</sup> Hexa ou Prevenar 13<sup>&reg;</sup>).
							</p>
						</div>

            		</div>
            		<div id="popupTolerance" class="feather">
            			<h2>Un bon profil de tol&eacute;rance</h2>
            			<div class="scroll">
							<h3>Analyse int&eacute;gr&eacute;e de la tol&eacute;rance des essais de primovaccination comparatifs &agrave; Infanrix<sup>&reg;</sup> Hexa R&eacute;actions &laquo; sollicit&eacute;es &raquo; apr&egrave;s primovaccination - Population de Tol&eacute;rance</h3>
							<img src="images/immunite_tab1.jpg" class="responsive" alt="Tableau d'analyse" width="895" height="402">
							<p>Analyse int&eacute;gr&eacute;e des essais comparatifs &agrave; Infanrix Hexa (A3L11, A3L12, A3L17, A3L24, A3L38)<br>Population de Tol&eacute;rance<br>
							R&eacute;actions pouvant &ecirc;tre rapport&eacute;es pour chacun des vaccins (Hexavalent ou Prevenar/Prevenar 13 en cas de co-administration)</p>

							<h3>Effets ind&eacute;sirables les plus fr&eacute;quemment rapport&eacute;s dans les essais cliniques et apr&egrave;s la mise sur le march&eacute;<sup>1</sup> :</h3>
							<h3 class="dark-blue">Syst&eacute;miques :</h3>
							<ul>
								<li>Anorexie (diminution de l'app&eacute;tit), pleurs, somnolence, vomissements, irritabilit&eacute;, fi&egrave;vre &ge; 38,0&deg;C</li>
								<li>Pleurs anormaux (prolong&eacute;s) et Diarrh&eacute;e</li>
							</ul>
							<h3 class="dark-blue">Au site d'injection :</h3>
							<ul>
								<li>Douleur, &eacute;ryth&egrave;me et &oelig;d&egrave;me</li>
								<li>Induration</li>
							</ul>
							<p>Une r&eacute;actog&eacute;nicit&eacute; sollicit&eacute;e l&eacute;g&egrave;rement sup&eacute;rieure a &eacute;t&eacute; observ&eacute;e apr&egrave;s la premi&egrave;re dose, par rapport aux doses suivantes.</p>
							<p>Pour plus d'information sur les effets ind&eacute;sirables peu fr&eacute;quents ou rares, consultez le R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</p>
							<p class="legend">1. R&eacute;sum&eacute; des Caract&eacute;ristiques du Produit.</p>

							<h3>Surveillance du profil de tol&eacute;rance</h3>
							<p>▼ Comme tout m&eacute;dicament contenant une nouvelle substance active autoris&eacute;e dans l&rsquo;Union Europ&eacute;enne, Hexyon fait l'objet  d&rsquo;une surveillance suppl&eacute;mentaire qui permettra l&rsquo;identification rapide de nouvelles informations relatives &agrave; la s&eacute;curit&eacute;.</p>
							<h3>Plan de Gestion des Risques</h3>
							<h3 class="dark-blue">Les &eacute;v&eacute;nements sous surveillance pour des raisons historiques ou Dits d'effet de classe sont :</h3>
							<ul>
								<li>Apn&eacute;e</li>
								<li>Enc&eacute;phalopathie, enc&eacute;phalite / enc&eacute;phalomy&eacute;lite aigu&euml; diss&eacute;min&eacute;e</li>
							</ul>
							<h3 class="dark-blue">Sont &eacute;galement surveill&eacute;s les risques dits importants :</h3>
							<ul>
								<li>&OElig;d&egrave;me &eacute;tendu du membre</li>
								<li>Episode d'hypotonie-hypor&eacute;activit&eacute;</li>
								<li>Convulsions avec ou sans fi&egrave;vre</li>
								<li>R&eacute;action anaphylactique</li>
							</ul>
						</div>

            		</div>
            	</div>
            	
            </div>

        </div>
    </div>
    <div class="footer">
    	<div class="column">
			<div class="col-left">
				<button id="references" class="footerBtn">R&eacute;f&eacute;rences</button>
				<div class="references">
					<ul class="no-bullets">
						<li class="no-bullet">1. Rapport d'&eacute;tude A3L38. EudraCT #2012-001054-26.</li>
					</ul>
				</div>
				<!--<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160829_Tolerance.pdf?Expires=1493798718&Signature=Dta6so5NIhkr33kwjAVhnLHrnFazubyiEbywJsH0qyPdOuUTRZKwjytveMjymGKQiaNohvZKyIq4fFyeZGUwIKa2mePA3w8c638TgFXrifQAeJLWQo8IQv9MtFkAJTlL-9oJdJZ2-FJANBWmu9BpBs7GtnHTf~wwpPUsfCJe2yE2rQvMH-b2vqm7SoMwrJVsmDTPhpF3bmxPixgDK89dzeZMYX-bCZIMZ7QO1kZiAaVciy32QOaUpnJX3gclPzz27Vnszg72~pn~kjO0hcej3vIEsQTGqUcgMTF9uiibDl9njiVfB5O7odApphRFCYCJewEf0lw0iK7C7D6zrJqF2A__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="tolerance" class="footerBtn">Tol&eacute;rance</a>-->
				<button id="tolerance" class="footerBtn" data-featherlight="#popupTolerance">Tol&eacute;rance</button>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_Recos-vaccinales.pdf?Expires=1493798718&Signature=mztc2Q5bom8Pl3EjYVnB1DzTLPFnex~-hj7SxbvOIqYFcW7BEiGW81oxcUbuVoL5s7~1~carcuyXC3HBA-70jYdZkPe4KMqjI0MWBftO1VX4tnFYMqcUXrgsI8JCB7vkYuJD9AfLfgec-ayVIVaOh~IkyUlcN3yWod8TJJ06nGZRnz11nGF82~A7pgL-BKtJ7ZYCVbLZldbCbOw4f8gnkquNoQ0h7IiYT~se16MLlgJKN3hQadryLHSF5edvlrC21sya3XBSoHdHVAz3LY2FR7N0qtyykfcxOyxlnm7z9sg2LrXGKc0VsSjgZPEdOc~pQ8fzwpUTQJnTotm3qjEHHw__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="recoVaccinales" class="footerBtn">Recommandations vaccinales</a>
				<a target="_blank" href="http://comms.univadis.com/s/spmsd/hexion/201611_edetail1/20160711_ML-Hexyon.pdf?Expires=1493798718&Signature=m-rf7wxL7LOVUf-oxzlCe6oCj0l5SxH9cXV9J2iDgzOSDLeUd0Im~BvfxSUyJUHJI~UlgxR40dFkNJR1eai5GxYPqTS4YUOES-yVwRPpkXg6ryWMbVbqNAU97enfuMjHTp5y03YuLEYUpWmSzGR8M9ETyaF~GeNRcJ~Ua0v7U5WTnZHXOwXZoptN2dbLrEFAGgsx1PEzJ5fOTc-MtothqwTzA5wgKMy1Q0mxr2yWqS6G2PTrmc5jsvHgl3gA~f2Tmla14MCrf9V9R52dDflwlPDJ1toi01~sMBW9EGL0~W3~njccfZPIFB-RjTatriEPknwnbU3vtdITcFp8JIoU1w__&Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" id="mentionsLegales" class="footerBtn">Mentions l&eacute;gales</a>
			</div>
			<div class="col-right">
				<img src="images/hexyon-logo.png" alt="Hexyon" width="142" height="41">
			</div>
		</div>
		<p>FR01411 - 16/07/60645557/PM/004 - FRAN000000979 - Juillet 2016</p>
	</div>
    
</section>